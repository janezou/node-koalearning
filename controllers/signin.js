// sign in:
const _ = require('lodash');

module.exports = {
  'POST /signin': async (ctx, next) => {
    var
      email = ctx.request.body.email || '',
      password = ctx.request.body.password || '';

    let result = await ctx.mongo.db('test').collection('users').findOne({ email: email, password: password });
    console.log(result);
    if (!_.isEmpty(result)) {
      ctx.redirect("/home");
      ctx.render('home.html', {
        title: 'Home',
        result: 1,
        tips: `Hi ${email}, welcome back! `
      });
    } else {
      ctx.redirect("/");
      ctx.render('index.html', {
        title: 'Welcome',
        result: 1,
        tips: "Fail to signin! "
      });
    }
    // if (email === 'admin@example.com' && password === '123456') {
    //   console.log('signin ok!');
    //   ctx.redirect("/home");
    //   ctx.render('home.html', {
    //     title: 'Home',
    //     name: email
    //   });
    // } else {
    //   console.log('signin failed!');
    //   ctx.render('index.html', {
    //     title: 'Welcome',
    //     result: 1,
    //     tips: "Fail to signin! "
    //   });
    // }
  }
};
// register:
const _ = require('lodash');

module.exports = {
  'GET /register': async (ctx, next) => {
    ctx.render('register.html', {
      title: 'Register',
      result: 0,
    });
  },
  'POST /register': async (ctx, next) => {
    var
      email = ctx.request.body.email || '',
      password = ctx.request.body.password || '';

    //
    let result = await ctx.mongo.db('test').collection('users').findOne({ email: email });
    console.log(result);
    if (!_.isEmpty(result)) {
      ctx.render('register.html', {
        title: 'Register',
        result: 1,
        tips: `Fail to register , This user(${email}) already exists! `
      });
    } else {
      result = await ctx.mongo.db('test').collection('users').insert({ email: email, password: password });
      console.log(result);
      if (result.ok === 1) {
        ctx.render('register.html', {
          title: 'Register',
          result: 1,
          tips: "Fail to register , please contact the administrator! "
        });
      } else {
        ctx.redirect("/");
        ctx.render('index.html', {
          title: 'Welcome',
          result: 1,
          tips: "Registration Successful !"
        });
      }
    }
  }
};
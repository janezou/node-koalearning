module.exports = {
  'GET /home': async (ctx, next) => {
    ctx.render('home.html', {
      title: 'Home'
    });
  }
};
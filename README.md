# 目的：学习koa框架

### #Project Requirements
Implement a RESTful API in Node.js for an AddressBook app following this specification.
The AddressBook app enables its users to manage a simple contact list by adding,
removing, and listing contacts.

#### User must be able to
- Create an account
- Log in
- Add a contact
- Remove a contact
- List all their contacts



### Your project must follow these requirements:
- Everything must be written in English
- Use NodeJS
- Code in JavaScript if you have never used NodeJS before
- Code in TypeScript if you have used NodeJS before
- Use Koa, not Express
- Store User accounts in an SQL database of your choice
- Store Contacts in a NoSQL database of your choice
- If none of these technologies are new to you, use at least one library or technology
- that you have not used before.
- Consider using automated testing
- Everything must be submitted to us under the MIT Licens

==If done, invite linsihong@codemao.cn to the repository.==

==For deployment you can use the free plan on Heroku.==
==Heroku has support for add-ons==
==so you can easily add databases of your choice.==

<!---->

# 参考网址：http://koa.bootcss.com/
# 参考网址：http://www.liaoxuefeng.com/wiki/001434446689867b27157e896e74d51a89c25cc8b43bdb3000/001434501628911140e1cb6ce7d42e5af81480f7ecd5802000
# 参考网址：http://www.runoob.com/mongodb/nosql.html
'use strict';

const koa = require('koa');
const app = new koa();
const convert = require('koa-convert');
const router = require('koa-router')();
const bodyParser = require('koa-bodyparser');
const controller = require('./controller');
const templating = require('./templating');
const mongo = require('./mongodb/mongodb');

var port = 3333;
var isProduction = false;

// logger
app.use(convert(function* (next) {
  const start = Date.now();
  yield next;
  const ms = Date.now() - start;
  console.log(`${this.method} ${this.url} - ${ms}ms`);
  console.log("=====================================================");
}));

app.use(bodyParser());

// logger2
app.use(convert(function* (next) {
  console.log(this.request.body);
  yield next;
  // console.log(this.response.body);
}));

// static file support:
if (!isProduction) {
  let staticFiles = require('./static-files');
  app.use(staticFiles('/bootstrap', __dirname + '/bootstrap'));
}


// response
// app.use(async (ctx, next) => {
//   // const user = await Users.getById(this.session.user_id);
//   await next();
//   ctx.body = { message: 'some message' };
// })

// add nunjucks as view:
app.use(templating('views', {
  noCache: !isProduction,
  watch: !isProduction
}));

// add mongo
app.use(mongo());

// add controller:
app.use(controller());

app.listen(port);
console.log();
console.log('The app is running at:');
console.log();
console.log('http://localhost:' + port + '/');
console.log();